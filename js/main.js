// Завдання
// Дано масив books.
//
//
//
// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const div = document.createElement("div");
div.id = "root";

const ul = document.createElement("ul");

// class Books {
//     constructor(options) {
//         this.author = options.author;
//         this.name = options.name;
//         this.price = options.price;
//     }
//
// }

const infoBooks = (books) => {


    books.forEach((book) => {
        let isBookValid = false;
        if (book.author === undefined) {
            console.error("author?");
        } else if (book.name === undefined) {
            console.error("name?");
        } else if (book.price === undefined) {
            console.error("price?");
        } else {
            isBookValid = true
        }
        if (isBookValid) {
            const li = document.createElement("li");
            const authorSpan = document.createElement("p");
            const nameSpan = document.createElement("p");
            const pricerSpan = document.createElement("p");
            authorSpan.innerText = book.author;
            nameSpan.innerText = book.name;
            pricerSpan.innerText = book.price;
            li.appendChild(authorSpan);
            li.appendChild(nameSpan);
            li.appendChild(pricerSpan);
            ul.appendChild(li);
        }

    });

};
infoBooks(books);
div.appendChild(ul);
document.body.appendChild(div);
// console.log(infoBooks);



